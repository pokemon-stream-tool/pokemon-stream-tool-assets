from pathlib import Path, PurePath
from urllib.parse import urlparse, urlunparse
from io import TextIOWrapper
from typing import Set
import time
import argparse
import logging
import htmllistparse

# Log level setting in CLI. From https://stackoverflow.com/a/65534541/.
parser = argparse.ArgumentParser()
parser.add_argument( '-log',
                     '--loglevel',
                     default='warning',
                     help='Provide logging level. Example --loglevel debug, default=warning' )

args = parser.parse_args()

logging.basicConfig( level=args.loglevel.upper() )
logging.info( 'Logging now setup.' )

added_urls: Set[str] = set()
def get_parent_url(url:str) -> str:
    parsed = urlparse(url)
    new_path = str(PurePath(parsed.path).parent)
    if not new_path.endswith('/'): new_path += '/' #Trailing slash.
    new_parsed = parsed._replace(path=new_path)
    return urlunparse(new_parsed)

wget_file = open("wgets.sh", "w+", encoding="utf-8")
wget_file.write("#!/usr/bin/bash\n") #Write the shebang line.
def times_not_within_tolerance(structtime: time.struct_time, unixtime: float, tolerance: int = 3600) -> bool:
    # Checks if the first time is at least "tolerance" seconds newer than the second time. (default is one hour).
    return time.mktime(structtime) - unixtime >= tolerance
def wget(url, file: TextIOWrapper = wget_file, mirror: bool = True, parent_url: str = "") -> None:
    if url in added_urls: return #Don't wget urls we're already wgetting anyway.
    command = f"{'#' if not mirror else ''}wget --execute=\"robots = off\" --convert-links {'--mirror ' if mirror else ' '*9}{'--no-parent ' if mirror else ' '*12}\"{url}\""
    #TODO: We have currently disabled downloading parent urls, since these download to the working directory. Maybe use -r -l1 for a compromise?
    print(f"we should `{command}`") #TODO: Aquí haríamos el wget.
    file.write(command+"\n")
    added_urls.add(url)
    if parent_url:
        wget(parent_url, file = file, mirror = False, parent_url = get_parent_url(parent_url))

def main_loop(url: str, spritedir: Path, cwd = None, listing = None) -> None:
    if cwd is None: # The main loop will initialize it, further ones will not.
        cwd, listing = htmllistparse.fetch_listing(url, timeout=30)
    for entry in listing:
        local_entry = spritedir.joinpath(entry.name)
        is_folder = entry.name.endswith("/")
        current_url = url + entry.name
        logging.info(f"{cwd}/{entry.name} {time.asctime(entry.modified)} {'folder' if is_folder else 'file'}")
        if not local_entry.exists():
            logging.warning(f"The path \"{local_entry}\" does not exist.")
            wget(current_url, parent_url = get_parent_url(current_url))
            continue
        #local_entry existe.
        is_outdated = times_not_within_tolerance(entry.modified, local_entry.stat().st_mtime)
        if is_folder: # Rehacemos el bucle, pero empezamos desde esa carpeta.
            sub_cwd, sub_listing = htmllistparse.fetch_listing(current_url, timeout=30)
            if sub_cwd is not None: # Solo empezamos el subbucle si es un directorio Apache. (No lo son las carpetas AFD, Trainers)
                main_loop(current_url, local_entry, cwd=sub_cwd, listing=sub_listing)
            elif is_outdated: # Si uno de los directorios no Apache está desactualizado, hacemos el wget entero.
                logging.warning(f"Directory \"{cwd + '/' + entry.name}\" is outdated. Server time: {time.asctime(entry.modified)}, Local time: {time.asctime(time.gmtime(local_entry.stat().st_mtime))}")
            continue
        #es un archivo (no carpeta).
        if is_outdated:
            logging.warning(f"File \"{cwd + '/' + entry.name}\" is outdated. Server time: {time.asctime(entry.modified)}, Local time: {time.asctime(time.gmtime(local_entry.stat().st_mtime))}")
            wget(current_url, parent_url = get_parent_url(current_url))
            continue

SPRITE_REPO = "https://play.pokemonshowdown.com/sprites/"
added_urls.update(("https://play.pokemonshowdown.com/", "https://play.pokemonshowdown.com/sprites/")) #So that we don't wget these ones.
BASE_LOCAL_SPRITEDIR = Path("./play.pokemonshowdown.com/sprites/")
SPRITE_DIRS = ("ani/", "ani-shiny/", "ani-back/", "ani-back-shiny/", "gen5/", "gen5-shiny/", "gen5-back/", "gen5-back-shiny/", "gen5ani/", "gen5ani-shiny/", "gen5ani-back/", "gen5ani-back-shiny/", "itemicons/")
for sprite_dir in SPRITE_DIRS:
    sprite_url = SPRITE_REPO + sprite_dir
    local_spritedir = BASE_LOCAL_SPRITEDIR.joinpath(sprite_dir)
    main_loop(sprite_url, local_spritedir)

#The next is for wgetting the icon sheets.

cwd, listing = htmllistparse.fetch_listing(SPRITE_REPO, timeout=30)
SHEETS = ("itemicons-sheet.png", "pokemonicons-sheet.png")
for sheet_name in SHEETS:
    sheet_entry = listing[[entry.name for entry in listing].index(sheet_name)]
    local_entry = BASE_LOCAL_SPRITEDIR.joinpath(sheet_name)
    if not local_entry.exists():
        logging.warning(f"The path \"{local_entry}\" does not exist.")
        wget(SPRITE_REPO + sheet_name)
        continue
    if times_not_within_tolerance(sheet_entry.modified, local_entry.stat().st_mtime):
        wget(SPRITE_REPO + sheet_name)

logging.debug(added_urls)
