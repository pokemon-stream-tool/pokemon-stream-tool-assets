import os, json, re, sys
from PIL import Image, GifImagePlugin, ImageFile, UnidentifiedImageError
import numpy as np
from glob import iglob
from itertools import chain

ImageFile.LOAD_TRUNCATED_IMAGES = True
GifImagePlugin.LOADING_STRATEGY = GifImagePlugin.LoadingStrategy.RGB_ALWAYS
sprites_path = os.path.join(".", "play.pokemonshowdown.com", "sprites")
output_filename = os.path.join(".", "offsets.json")

def calculate_centers(image: Image):
    center = np.asarray([image.width/2, image.height/2])

    average_center_of_mass = np.zeros(2)
    if isinstance(image, GifImagePlugin.GifImageFile):
        for i in range(image.n_frames):
            image.seek(i)
            image_array = np.asarray(image.getchannel("A"))
            center_of_mass = [ np.average(indices) for indices in np.where(image_array > 0) ][::-1] #numpy axis order is the opposite of the usual one.
            average_center_of_mass += center_of_mass
        average_center_of_mass = average_center_of_mass/image.n_frames
    else:
        image_array = np.asarray(image.convert(mode="RGBA").getchannel("A"))
        average_center_of_mass = [ np.average(indices) for indices in np.where(image_array > 0) ][::-1]

    return center, average_center_of_mass

warnings = []
sprite_dict = {}
filetypes = ("png", "gif") #offsets should probably not be used with png files, but might as well calculate those too.
glob_iterator = chain(*[iglob(f"*/*.{filetype}", root_dir=sprites_path, recursive=True) for filetype in filetypes])
for image_file in glob_iterator:
    try:
        image = Image.open(os.path.join(sprites_path, image_file))
    except UnidentifiedImageError as e:
        warnings.append(e)
        print(e, file=sys.stderr)
        continue
    center, average_center_of_mass = calculate_centers(image)
    offset = [round(x, 2) for x in (center - average_center_of_mass)]
    print(image_file, offset)
    sprite_dict[image_file] = offset

class CustomJSONEncoder(json.JSONEncoder): #print names and offsets in one line. VERY janky code.
    #I really should have just made a custom "dict to string" function instead of this mess lol
    regex = re.compile("\d|\[|\]")
    regex2 = re.compile("\n")
    def iterencode(self, obj, **kwargs):
        prev = None
        for encoded in super(CustomJSONEncoder, self).iterencode(obj, **kwargs):
            if self.regex.search(encoded) or (self.regex2.match(encoded) and self.regex.search(prev)):
                encoded = encoded.replace(" ", "").replace("\n", " ")
            if encoded == "}":
                encoded = "\n}"
            prev = encoded
            yield encoded
        

with open(output_filename, "w") as output_file:
    json.dump(sprite_dict, output_file, cls=CustomJSONEncoder, sort_keys=True, indent=4)

for e in warnings:
    print(e, file=sys.stderr)
