# Pokémon Stream Tool Assets

Assets for the [Pokémon Stream Tool](https://github.com/Readek/Pokemon-Stream-Tool).

Currently, the assets are fetched by `cd`ing into the repo's root folder and running:
```sh
    wget --mirror --no-parent --input-file wget_url_list.txt
    python offset_json_generator.py
```

Future releases might include hand-picked assets or manual offset changes.
